package demo.paging.androidx.androidxpagingjava.viewHolder;

import androidx.recyclerview.widget.RecyclerView;
import demo.paging.androidx.androidxpagingjava.databinding.ItemNetworkStateBinding;
import demo.paging.androidx.androidxpagingjava.inf.RetryCallback;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import demo.paging.androidx.androidxpagingjava.models.NetworkStateItemViewModel;

public class NetworkStateViewHolder extends RecyclerView.ViewHolder {

    private ItemNetworkStateBinding mBinding;

    private NetworkStateItemViewModel mNetworkStateItemViewModel;

    private RetryCallback mRetryCallback;

    public NetworkStateViewHolder(ItemNetworkStateBinding binding, RetryCallback retryCallback) {
        super(binding.getRoot());
        this.mBinding = binding;
        this.mRetryCallback = retryCallback;
    }

    public void bindTo(NetworkState networkState) {
        mNetworkStateItemViewModel = new NetworkStateItemViewModel(networkState, mRetryCallback);
        mBinding.setViewModel(mNetworkStateItemViewModel);
        // Immediate Binding
        // When a variable or observable changes, the binding will be scheduled to change before
        // the next frame. There are times, however, when binding must be executed immediately.
        // To force execution, use the executePendingBindings() method.
        mBinding.executePendingBindings();


    }

}
