package demo.paging.androidx.androidxpagingjava.models;

/**
 * Created by Ahmed Abd-Elmeged on 2/13/2018.
 */
public enum Status {
    RUNNING,
    SUCCESS,
    FAILED,
    COMPLETED
}
