package demo.paging.androidx.androidxpagingjava.paging.parent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import demo.paging.androidx.androidxpagingjava.R;
import demo.paging.androidx.androidxpagingjava.databinding.ItemNetworkStateBinding;
import demo.paging.androidx.androidxpagingjava.inf.RetryCallback;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import demo.paging.androidx.androidxpagingjava.viewHolder.NetworkStateViewHolder;


public abstract class RecyclerParent<T> extends PagedListAdapter<T, RecyclerView.ViewHolder> {

    private NetworkState networkState = null;
    private RetryCallback retryCallback;

    protected RecyclerParent(@NonNull DiffUtil.ItemCallback<T> diffCallback, RetryCallback retryCallback) {
        super(diffCallback);
        this.retryCallback = retryCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == R.layout.item_network_state) {
            ItemNetworkStateBinding view = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_network_state, viewGroup, false);
            return new NetworkStateViewHolder(view, retryCallback);
        }
        return null;
    }

    public void onBindViewHolderSuper(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof NetworkStateViewHolder) {
            bindNetworkView((NetworkStateViewHolder) viewHolder, position, viewHolder.itemView.getContext());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1)
            return R.layout.item_network_state;
        return 0;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + (hasExtraRow() ? 1 : 0);
    }

    protected boolean hasExtraRow() {
        if (networkState != null) {
            if (networkState == NetworkState.COMPLETED)
                return true;
            return networkState != NetworkState.LOADED;
        }
        return false;
    }

    public void setNetworkState(NetworkState newNetworkState) {
        if (getCurrentList() != null) {
            if (getCurrentList().size() != 0) {
                NetworkState previousState = this.networkState;
                boolean hadExtraRow = hasExtraRow();
                this.networkState = newNetworkState;
                boolean hasExtraRow = hasExtraRow();
                if (hadExtraRow != hasExtraRow) {
                    if (hadExtraRow) {
                        notifyItemRemoved(super.getItemCount());
                    } else {
                        notifyItemInserted(super.getItemCount());
                    }
                } else if (hasExtraRow && previousState != newNetworkState) {
                    notifyItemChanged(getItemCount() - 1);
                }
            }
        }
    }

    private void bindNetworkView(NetworkStateViewHolder holder, int position, Context context) {
        holder.bindTo(networkState);
    }

    public static GridLayoutManager getGridLayoutManager(Context context, int span, RecyclerParent<?> adapter) {
        GridLayoutManager layoutManager = new GridLayoutManager(context, span);
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (adapter != null) {
                    int itemView = adapter.getItemViewType(position);
                    if (itemView == R.layout.item_network_state) {
                        return span;
                    }
                }
                return 1;
            }
        });
        return layoutManager;
    }
}
