package demo.paging.androidx.androidxpagingjava.inf;

public interface RetryCallback {
    void retry();
}