package demo.paging.androidx.androidxpagingjava.models;

import java.util.logging.Logger;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import demo.paging.androidx.androidxpagingjava.inf.RetryCallback;

public class NetworkStateItemViewModel {

    public final ObservableBoolean isErrorMessageVisible;
    public final ObservableBoolean isRetryButtonVisible;
    public final ObservableBoolean isLoadingProgressBarVisible;
    public final ObservableBoolean isCompletedLoading;
    public final ObservableField<String> errorMessage;

    private final NetworkState mNetworkState;
    private final RetryCallback mRetryCallback;

    public NetworkStateItemViewModel(NetworkState networkState, RetryCallback retryCallback) {
        this.mNetworkState = networkState;
        this.mRetryCallback = retryCallback;
        isErrorMessageVisible = new ObservableBoolean(mNetworkState.getMessage() != null ? true : false);
        errorMessage = new ObservableField<>(mNetworkState.getMessage());

        isRetryButtonVisible = new ObservableBoolean(mNetworkState.getStatus() == Status.FAILED ? true : false);
        isCompletedLoading = new ObservableBoolean(mNetworkState.getStatus() == Status.COMPLETED ? true : false);
        isLoadingProgressBarVisible = new ObservableBoolean(mNetworkState.getStatus() == Status.RUNNING ? true : false);
    }

    public void onClickRetryButton(){
        mRetryCallback.retry();
    }

}