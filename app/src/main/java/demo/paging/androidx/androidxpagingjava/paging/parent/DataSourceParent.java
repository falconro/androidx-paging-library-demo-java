package demo.paging.androidx.androidxpagingjava.paging.parent;


import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;
import demo.paging.androidx.androidxpagingjava.helpers.Utils;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import retrofit2.Call;


public abstract class DataSourceParent<I, P> extends PageKeyedDataSource<I, P> {

    public List<Call<?>> compositeDisposable;
    public CompositeDisposable compositeDisposableData;

    private MutableLiveData<NetworkState> networkState = new MutableLiveData<>();
    private MutableLiveData<NetworkState> initialLoad = new MutableLiveData<>();

    private Completable retryCompletable = null;

    public DataSourceParent(List<Call<?>> compositeDisposable, CompositeDisposable compositeDisposableData) {
        this.compositeDisposable = compositeDisposable;
        this.compositeDisposableData = compositeDisposableData;
    }

    public void retry() {
        if (retryCompletable != null) {
            compositeDisposableData.add(retryCompletable.subscribeOn(Utils.getSubscriber()).observeOn(Utils.getObserver()).subscribe(() -> {
            }, throwable -> {

            }));
        }
    }

    public void setRetry(Action action) {
        if (action == null) {
            this.retryCompletable = null;
        } else {
            this.retryCompletable = Completable.fromAction(action);
        }
    }

    protected void loadInitialPre() {
        getNetworkState().postValue(NetworkState.LOADING);
        getInitialLoad().postValue(NetworkState.LOADING);
    }

    protected void loadInitialCompleted() {
        setRetry(null);
        getNetworkState().postValue(NetworkState.LOADED);
        getInitialLoad().postValue(NetworkState.LOADED);
    }

    protected void loadInitialError(String msg) {
        NetworkState error = NetworkState.error(msg);
        getNetworkState().postValue(error);
        getInitialLoad().postValue(error);
    }


    protected void loadAfterPre() {
        getNetworkState().postValue(NetworkState.LOADING);
    }

    protected void loadAfterComplete() {
        setRetry(null);
        getNetworkState().postValue(NetworkState.LOADED);
    }

    protected void loadAfterError(String msg) {
        getNetworkState().postValue(NetworkState.error(msg));
    }

    protected void loadNextKey(Integer next) {
        if (next == null) {
            getNetworkState().postValue(NetworkState.COMPLETED);
        }
    }

    @NonNull
    public MutableLiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    @NonNull
    public MutableLiveData<NetworkState> getInitialLoad() {
        return initialLoad;
    }
}
