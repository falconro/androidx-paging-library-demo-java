package demo.paging.androidx.androidxpagingjava.paging;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import demo.paging.androidx.androidxpagingjava.models.PageItem;
import demo.paging.androidx.androidxpagingjava.paging.parent.SourceFactoryParent;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;

public class PageDataSourceFactory extends SourceFactoryParent<Integer, PageItem> {

    private MutableLiveData<PageDataSource> mDataSourceLiveData = new MutableLiveData<>();

    public PageDataSourceFactory(List<Call<?>> compositeDisposable, CompositeDisposable compositeDisposableData) {
        super(compositeDisposable, compositeDisposableData);
    }

    @Override
    public DataSource<Integer, PageItem> create() {
        PageDataSource source = new PageDataSource(compositeDisposable,compositeDisposableData);
        mDataSourceLiveData.postValue(source);
        return source;
    }

    @NonNull
    public MutableLiveData<PageDataSource> getUsersDataSourceLiveData() {
        return mDataSourceLiveData;
    }
}
