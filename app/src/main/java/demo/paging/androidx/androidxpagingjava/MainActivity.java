package demo.paging.androidx.androidxpagingjava;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import demo.paging.androidx.androidxpagingjava.databinding.ActivityMainBinding;
import demo.paging.androidx.androidxpagingjava.inf.RetryCallback;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import demo.paging.androidx.androidxpagingjava.paging.ListViewModel;
import demo.paging.androidx.androidxpagingjava.paging.PageAdapter;
import demo.paging.androidx.androidxpagingjava.paging.parent.RecyclerParent;
import demo.paging.androidx.androidxpagingjava.viewmodel_provider.ViewModelProviderFactory;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity implements RetryCallback {

    private ViewModelProviderFactory<ListViewModel> viewModelFactory;
    private ListViewModel viewModel;
    private PageAdapter adapter;
    private ActivityMainBinding binding;
    public CompositeDisposable retrofitList = new CompositeDisposable();
    public List<Call<?>> retrofitListCall = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        viewModelFactory = new ViewModelProviderFactory<>(new ListViewModel(retrofitListCall, retrofitList));
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ListViewModel.class);
        binding.content.setViewModel(viewModel);
        binding.content.executePendingBindings();


        adapter = new PageAdapter(this);
        RecyclerView recyclerView = binding.content.recycler;

        recyclerView.setLayoutManager(RecyclerParent.getGridLayoutManager(this, 2, adapter));
        recyclerView.setAdapter(adapter);
        viewModel.list.observe(this, adapter::submitList);
        viewModel.getNetworkState().observe(this, adapter::setNetworkState);

        initSwipeToRefresh();
    }

    private void initSwipeToRefresh() {
        viewModel.getRefreshState().observe(this, networkState -> {
            if (networkState != null) {
                if (adapter.getCurrentList() != null) {
                    if (adapter.getCurrentList().size() > 0) {
                        binding.content.usersSwipeRefreshLayout.setRefreshing(networkState.getStatus() == NetworkState.LOADING.getStatus());
                    }
                    setInitialLoadingState(networkState);
                } else {
                    setInitialLoadingState(networkState);
                }
            }
        });
    }

    private void setInitialLoadingState(NetworkState networkState) {
        viewModel.setInitialLoadingState(networkState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void retry() {
        viewModel.retry();
    }
}
