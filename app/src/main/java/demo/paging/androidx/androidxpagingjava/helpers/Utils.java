package demo.paging.androidx.androidxpagingjava.helpers;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Utils {
    public static Scheduler getSubscriber() {
        return Schedulers.io();
    }

    public static Scheduler getObserver() {
        return AndroidSchedulers.mainThread();
    }
}
