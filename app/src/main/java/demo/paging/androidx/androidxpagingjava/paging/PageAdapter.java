package demo.paging.androidx.androidxpagingjava.paging;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import demo.paging.androidx.androidxpagingjava.R;
import demo.paging.androidx.androidxpagingjava.databinding.ItemNetworkStateBinding;
import demo.paging.androidx.androidxpagingjava.inf.RetryCallback;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import demo.paging.androidx.androidxpagingjava.models.PageItem;
import demo.paging.androidx.androidxpagingjava.paging.parent.RecyclerParent;
import demo.paging.androidx.androidxpagingjava.viewHolder.NetworkStateViewHolder;

public class PageAdapter extends RecyclerParent<PageItem> {

    private final RetryCallback retryCallback;
    private NetworkState networkState;

    public PageAdapter(RetryCallback retryCallback) {
        super(diffCallback,retryCallback);
        this.retryCallback = retryCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolderParent = super.onCreateViewHolder(viewGroup, viewType);
        if (viewHolderParent != null)
            return viewHolderParent;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.page_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        onBindViewHolderSuper(viewHolder, position);
        if (viewHolder instanceof ViewHolder) {
            ((ViewHolder) viewHolder).bindTo(getItem(position),position);
        }
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
        }

        public void bindTo(PageItem item, int position) {
            text.setText("Page: " + item.id + " | ID:" + position);
        }
    }


    private static final DiffUtil.ItemCallback<PageItem> diffCallback = new DiffUtil.ItemCallback<PageItem>() {
        @Override
        public boolean areItemsTheSame(@NonNull PageItem pageItem, @NonNull PageItem t1) {
            return pageItem.id == t1.id;
        }

        @Override
        public boolean areContentsTheSame(@NonNull PageItem pageItem, @NonNull PageItem t1) {
            return pageItem.equals(t1);
        }
    };
}
