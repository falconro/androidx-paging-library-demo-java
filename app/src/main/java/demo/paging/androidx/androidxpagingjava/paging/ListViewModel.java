package demo.paging.androidx.androidxpagingjava.paging;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import demo.paging.androidx.androidxpagingjava.models.PageItem;
import demo.paging.androidx.androidxpagingjava.paging.parent.ViewModelParent;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;

public class ListViewModel extends ViewModelParent {


    public static final int PAGE_SIZE = 9;
    public final LiveData<PagedList<PageItem>> list;
    private PageDataSourceFactory sourceFactory;


    @Override
    protected int pageSize() {
        return PAGE_SIZE;
    }

    public ListViewModel(List<Call<?>> compositeDisposable, CompositeDisposable compositeDisposableData) {
        sourceFactory = new PageDataSourceFactory(compositeDisposable, compositeDisposableData);
        list = new LivePagedListBuilder<>(sourceFactory, getConfig()).build();
    }

    @Override
    public void retry() {
        sourceFactory.getUsersDataSourceLiveData().getValue().retry();
    }

    @Override
    public void refresh() {
        sourceFactory.getUsersDataSourceLiveData().getValue().invalidate();
    }

    @Override
    public LiveData<NetworkState> getNetworkState() {
        return Transformations.switchMap(sourceFactory.getUsersDataSourceLiveData(), PageDataSource::getNetworkState);
    }

    @Override
    public LiveData<NetworkState> getRefreshState() {
        return Transformations.switchMap(sourceFactory.getUsersDataSourceLiveData(), PageDataSource::getInitialLoad);
    }
}
