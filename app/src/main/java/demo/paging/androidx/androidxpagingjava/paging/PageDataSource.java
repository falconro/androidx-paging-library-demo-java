package demo.paging.androidx.androidxpagingjava.paging;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import demo.paging.androidx.androidxpagingjava.models.PageItem;
import demo.paging.androidx.androidxpagingjava.paging.parent.DataSourceParent;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;

public class PageDataSource extends DataSourceParent<Integer, PageItem> {


    public PageDataSource(List<Call<?>> compositeDisposable, CompositeDisposable compositeDisposableData) {
        super(compositeDisposable, compositeDisposableData);
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, PageItem> callback) {
        loadInitialPre();

        //replace api call here
        getPageData(1, new dataSourceCallback() {
            @Override
            public void listItems(List<PageItem> list, Integer nextKey) {
                loadInitialCompleted();
                loadNextKey(list.size() != ListViewModel.PAGE_SIZE ? null : nextKey);
                callback.onResult(list, 1, list.size() != ListViewModel.PAGE_SIZE ? null : nextKey);
            }

            @Override
            public void error(String msg) {
                loadInitialError(msg);

                setRetry(() -> loadInitial(params, callback));
            }
        });
    }


    private void getPageData(int i, dataSourceCallback callback) {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<PageItem> list = new ArrayList<>();
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        list.add(new PageItem(i));
        if (i == 5) {
            //callback.listItems(list, null);
            callback.error("xxxxx");
        } else {
            callback.listItems(list, i + 1);
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, PageItem> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull final LoadCallback<Integer, PageItem> callback) {
        //replace api call here
        loadAfterPre();

        getPageData(params.key, new dataSourceCallback() {
            @Override
            public void listItems(List<PageItem> list, Integer nextKey) {
                loadAfterComplete();
                loadNextKey(nextKey);
                callback.onResult(list, nextKey);
            }

            @Override
            public void error(String msg) {
                loadAfterError(msg);

                setRetry(() -> loadAfter(params, callback));
            }
        });
    }


    public interface dataSourceCallback {
        void listItems(List<PageItem> list, Integer nextKey);

        void error(String msg);
    }
}
