package demo.paging.androidx.androidxpagingjava.paging.parent;




import java.util.ArrayList;
import java.util.List;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedList;
import demo.paging.androidx.androidxpagingjava.helpers.RetrofitStatic;
import demo.paging.androidx.androidxpagingjava.models.NetworkState;
import demo.paging.androidx.androidxpagingjava.models.Status;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;


public abstract class ViewModelParent extends ViewModel {

    public ObservableBoolean isErrorMessageVisible;
    public ObservableBoolean isRetryButtonVisible;
    public ObservableBoolean isLoadingProgressBarVisible;
    public ObservableBoolean isSwipeRefreshLayoutEnable;
    public ObservableField<String> errorMessage;
    public List<Call<?>> compositeDisposable = new ArrayList<>();
    public CompositeDisposable compositeDisposableData = new CompositeDisposable();

    public ViewModelParent() {
        isErrorMessageVisible = new ObservableBoolean(false);
        errorMessage = new ObservableField<>("");
        isRetryButtonVisible = new ObservableBoolean(false);
        isLoadingProgressBarVisible = new ObservableBoolean(true);
        isSwipeRefreshLayoutEnable = new ObservableBoolean(true);
    }

    protected int pageSize() {
        return 10;
    }

    public PagedList.Config getConfig() {
        return new PagedList.Config.Builder()
                .setPageSize(pageSize())
                .setEnablePlaceholders(false)
                .build();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        RetrofitStatic.clearRetrofitList(compositeDisposable);
        compositeDisposableData.clear();
    }

    public void setInitialLoadingState(NetworkState networkState) {
        isErrorMessageVisible.set((networkState.getMessage() != null));
        errorMessage.set(networkState.getMessage());
        isRetryButtonVisible.set(networkState.getStatus() == Status.FAILED);
        isLoadingProgressBarVisible.set(networkState.getStatus() == Status.RUNNING);
        isSwipeRefreshLayoutEnable.set(networkState.getStatus() == Status.SUCCESS);
    }

    public void retry() {
    }

    public void refresh() {
    }

    public LiveData<NetworkState> getNetworkState() {
        return null;
    }

    public LiveData<NetworkState> getRefreshState() {
        return null;
    }
}
