package demo.paging.androidx.androidxpagingjava.paging.parent;


import java.util.List;

import androidx.paging.DataSource;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;


public abstract class SourceFactoryParent<I, P> extends DataSource.Factory<I, P> {

    public CompositeDisposable compositeDisposableData;
    public List<Call<?>> compositeDisposable;

    public SourceFactoryParent(List<Call<?>> compositeDisposable, CompositeDisposable compositeDisposableData) {
        this.compositeDisposable = compositeDisposable;
        this.compositeDisposableData = compositeDisposableData;
    }
}
